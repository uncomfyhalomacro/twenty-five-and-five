# It's a simple app but it's in iced-rs

NOTE to self: *Check stopwatch example*

## Our first goal of course is creating a window

- [X] Creating the main window

## Then create the buttons and display window for the time

- [ ] Buttons
  - [X] Start
  - [X] Pause
  - [X] Reset
  - [ ] Edit
- [X] Display Window
  - [X] Left
  - [X] Middle
  - [X] Right

## Then notifications

- [X] Notify for Starting
- [X] Notify for Finished

# Timer

- [X] Pause and start
- [X] Store remaining seconds if paused